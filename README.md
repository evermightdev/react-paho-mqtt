A simple project to test an mqtt client for React.  Ultimately I just used the paho-mqtt package and built my own react wrapper around it

The latest version simply uses `src/App.js`.  I have a few other `src/App*.js` that I was experimenting with that you can also try out.

You can run the project with the basic `npm install` command to download dependencies.

Run `npm start`.

Here's an OLDER video I made to remind myself of how I got here:

https://www.youtube.com/watch?v=h4PHiwHUmlM
