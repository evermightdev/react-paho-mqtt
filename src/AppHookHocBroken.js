import React, {useState,createContext} from 'react';
import Paho from 'paho-mqtt';

const Ctx = createContext({});

const Connect = props => {
  const {serverName,port,topic,clientName} = props;
  const [data,setMessage] = useState(0);

  const client = new Paho.Client(serverName,port,clientName);

  client.onMessageArrived = message=>{
    data.push(message.payloadString);
    setMessage(data);
  };

  client.connect({onSuccess:_=>{
    client.subscribe(topic);
  }});

  return <Ctx.Provider data={data}>{props.children}</Ctx.Provider>;
}

const useConnect = WrappedComponent => props =>{
  const data = useContext(Ctx);
  return <WrapedComponent data={data} />
};

const App = props => {
  return props && props.data && props.data.map(item=><div>{item}</div>);
}
//const AppConnect = Connect(App,"test.mosquitto.org",8080,"test","WebBrowser");
//const AppConnect = Connect({WrappedComponent:App,
//  serverName:"test.mosquitto.org",port:8080,topic:"test",clientName:"WebBrowser"});
//const AppConnect = useConnect(App,
export default AppConnect;
