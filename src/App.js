import React from 'react';
import Paho from 'paho-mqtt';


function Connect(WrappedComponent,serverName,port,topic,clientName) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data:[],
      };
      const client = new Paho.Client(serverName,port,clientName);

      client.onConnectionLost = this.onConnectionLost.bind(this);
      client.onMessageArrived = this.onMessageArrived.bind(this);

      client.connect({
        onSuccess:_=>{
        client.subscribe(topic);
        },
        useSSL:true,
        userName:"anzen",
        password:"#communicate-bms#",
      });

    }
    onMessageArrived(message) {
      console.log(message.payloadBytes);
      console.log(message.payloadString);

      const {data} = this.state;
      data.push(message.payloadString);
      this.setState({data});
    }
    onConnectionLost(responseObject) {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
      }
    }
    render() {
      return <WrappedComponent data={this.state.data} />;
    }
  };
}


function App(props) {
  return props && props.data && props.data.map(item=><div>{item}</div>);
}
const AppConnect = Connect(App,"dev.kpmpower.com",9102,"test","WebBrowser");
export default AppConnect;
