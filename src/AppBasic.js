import React from 'react';
//import {Paho} from './Library/mqttws31min';
import Paho from 'paho-mqtt';


var topic = "test";
let app;

function connect(component) {
  app = component;
  // Create a client instance
  //client = new Paho.MQTT.Client(location.hostname, Number(location.port), "WebBrowser");
  //client = new Paho.MQTT.Client(location.hostname,9001, "WebBrowser");
  const client = new Paho.Client("test.mosquitto.org",8080, "WebBrowser");

  // set callback handlers
  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived;

  // connect the client
  client.connect({onSuccess:_=>onConnect(client)});
}


// called when the client connects
function onConnect(client) {
  // Once a connection has been made, make a subscription and send a message.
  client.subscribe(topic);
  /*
  message = new Paho.MQTT.Message("Hello afe");
  message.destinationName = topic;
  client.send(message);
  */
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  console.log(message.payloadBytes);
  console.log(message.payloadString);

  const {data} = app.state;
  data.push(message.payloadString);
  app.setState({data});
  //location.href = location.href;
}


//const srv = "ws://learn.evermight.net:9001";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[],
    };
    connect(this);
  }
  render() {
    return this.state.data.map(item=><div>{item}</div>);
  }
}
export default App;
